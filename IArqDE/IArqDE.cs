﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace IArqDE
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IArqDE
    {
        //teste
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "json/{id}")]
        string JSONData(string id);

        #region GET
        
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest, Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetFileVersion/{numero}/{filename}/{version}")]
        [OperationContract]
        Stream GetFileVersion(string numero, string filename, string version);

        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest, Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetFile/{numero}/{filename}")]
        [OperationContract]
        Stream GetFile(string numero, string filename);

        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest, Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetFileListVersion/{numero}")]
        [OperationContract]
        List<FileVersionsItem> GetFileListVersion(string numero);

        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest, Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetFileList/{numero}")]
        [OperationContract]
        List<ProcessoAnexo> GetFileList(string numero);

        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest, Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAlunoFromSIGACAD/{numero}")]
        [OperationContract]
        List<AlunoItem> GetAlunoFromSIGACAD(string numero);

        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest, Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetOutrosCursos")]
        [OperationContract]
        List<OutrosCursos> GetOutrosCursos();

        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest, Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetTiposProcessos/{app}")]
        [OperationContract]
        List<ProcessosArqdeItem> GetTiposProcessos(string app);

        #endregion

        #region POST

        #region GENERICO

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest, Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "InsertProcessoEstudante")]
        Result InsertProcessoEstudante(Stream request);


        //InsertAnexoProcessoEstudante
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest, Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "InsertAnexoProcessoEstudante/{numInf}/{login}/{nomeAnexo}")]
        Result InsertAnexoProcessoEstudante(Stream anexo, string numInf, string login, string nomeAnexo);

        #endregion

        #region REQUERIMENTOS
        //[OperationContract]
        //[WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest, Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "InsertInformacaoRequerimento/{curso}/{assunto}/{texto}/{numMec}/{nome}/{loginCriadoPor}/{nomeCriadoPor}")]
        //Result InsertInformacaoRequerimento(string curso, string assunto, string texto, string numMec, string nome, string loginCriadoPor, string nomeCriadoPor);


        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest, Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "InsertInformacaoRequerimento")]
        Result InsertInformacaoRequerimento(Stream request);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest, Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "InsertAnexoRequerimento/{numInf}/{login}/{nomeAnexo}")]
        Result InsertAnexoRequerimento(Stream anexo, string numInf, string login, string nomeAnexo);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest, Method = "POST", ResponseFormat = WebMessageFormat.Json, UriTemplate = "UpdateAnexoRequerimento/{numInf}/{login}/{nomeAnexo}")]
        Result UpdateAnexoRequerimento(Stream anexo, string numInf, string login, string nomeAnexo);

        #endregion

        #endregion
    }

    
}
