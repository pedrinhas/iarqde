﻿using System;
using System.Configuration;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;

namespace IArqDE
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes();
        }

        private void RegisterRoutes()
        {
            // Edit the base address of Service1 by replacing the "Service1" string below
            // RouteTable.Routes.Add(new ServiceRoute("Service1", new WebServiceHostFactory(), typeof(Service1)));
            //RouteTable.Routes.Add(new ServiceRoute("GesDoc", new WebServiceHostFactory(), typeof(GesDoc)));
            RouteTable.Routes.Add((RouteBase)new ServiceRoute("ArqDE", (ServiceHostFactoryBase)new WebServiceHostFactory(), typeof(ArqDE)));



            SharePointTools.SharepointTools.User = ConfigurationManager.AppSettings["user"];
            SharePointTools.SharepointTools.Password = ConfigurationManager.AppSettings["password"].ToSecureString();
            SharePointTools.SharepointTools.Domain = ConfigurationManager.AppSettings["domain"];
        }
    }
}
