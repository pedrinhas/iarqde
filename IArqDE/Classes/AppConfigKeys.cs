﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IArqDE
{
    public static class AppConfig
    {
        public static String SharepointApplicationsUrl
        {
            get { return Prod ? ConfigurationManager.AppSettings["sharepointConfigAddress2013Prod"] : ConfigurationManager.AppSettings["sharepointConfigAddress2013Dev"]; }
        }
        public static String SharepointReitor
        {
            get { return ConfigurationManager.AppSettings["sharepointReitoria"]; }
        }public static String SharepointReitorDev
        {
            get { return ConfigurationManager.AppSettings["sharepointReitoriaDev"]; }
        }
        public static String ListNameProcessos
        {
            get { return ConfigurationManager.AppSettings["listNameProcessos"]; }
        }
        public static String ListNamePautas
        {
            get { return ConfigurationManager.AppSettings["listNamePautas"]; }
        }
        public static String ListNameDespachosReitorais
        {
            get { return ConfigurationManager.AppSettings["listNameDespachosReitorais"]; }
        }
        public static String Domain
        {
            get { return ConfigurationManager.AppSettings["domain"]; }
        }
        public static String User
        {
            get { return ConfigurationManager.AppSettings["user"]; }
        }
        public static String Password
        {
            get { return ConfigurationManager.AppSettings["password"]; }
        }
        public static bool Debug
        {
            get { return ConfigurationManager.AppSettings["debug"].ToString() == "true"; }
        }
        public static bool Prod
        {
            get { return ConfigurationManager.AppSettings["prod"].ToString() == "true"; }
        }
        public static string Obs
        {
            get { return ConfigurationManager.AppSettings["obs"].ToString(); }
        }

        public static class ConnectionStrings
        {
            public static string GesDoc
            {
                get { return System.Configuration.ConfigurationManager.ConnectionStrings["gesDoc"].ConnectionString; }
            }
        }
        public static class ContentTypes
        {
            public static String Processos
            {
                get { return ConfigurationManager.AppSettings["CTProcessos"]; }
            }
            public static String Pautas
            {
                get { return ConfigurationManager.AppSettings["CTPautas"]; }
            }
            [Obsolete("Content type do SharePoint 2013, em princípio não vai ser usado", true)]
            public static String DRPublicacao
            {
                get { return ConfigurationManager.AppSettings["CTDRPublicacao"]; }
            }
            [Obsolete("Content type do SharePoint 2013, em princípio não vai ser usado", true)]
            public static String DRTeste
            {
                get { return ConfigurationManager.AppSettings["CTDRTeste"]; }
            }
            public static String DespachosReitorais
            {
                get { return ConfigurationManager.AppSettings["CTDespachosReitorais"]; }
            }
        }
        
        public static class Headers
        {
            public static string FileName { get { return ConfigurationManager.AppSettings["Headers.FileName"]; } }
            public static string ProcessNumber { get { return ConfigurationManager.AppSettings["Headers.ProcessNumber"]; } }
            public static string Token { get { return ConfigurationManager.AppSettings["Headers.Token"]; } }
            public static string Categoria { get { return ConfigurationManager.AppSettings["Headers.Categoria"]; } }
            public static string Numero { get { return ConfigurationManager.AppSettings["Headers.Numero"]; } }
            public static string AnoLetivo { get { return ConfigurationManager.AppSettings["Headers.AnoLetivo"]; } }
            public static string Curso { get { return ConfigurationManager.AppSettings["Headers.Curso"]; } }
            public static string Data { get { return ConfigurationManager.AppSettings["Headers.Data"]; } }
            public static string Docente { get { return ConfigurationManager.AppSettings["Headers.Docente"]; } }
            public static string Epoca { get { return ConfigurationManager.AppSettings["Headers.Epoca"]; } }
            public static string UC { get { return ConfigurationManager.AppSettings["Headers.UC"]; } }
            public static string UsernameDocente { get { return ConfigurationManager.AppSettings["Headers.UsernameDocente"]; } }
            public static string Assinado { get { return ConfigurationManager.AppSettings["Headers.Assinado"]; } }
            public static string Titulo { get { return ConfigurationManager.AppSettings["Headers.Titulo"]; } }
            public static string Texto { get { return ConfigurationManager.AppSettings["Headers.Texto"]; } }
            public static string Destaque { get { return ConfigurationManager.AppSettings["Headers.Destaque"]; } }
            public static string Ano { get { return ConfigurationManager.AppSettings["Headers.Ano"]; } }
            public static string Publicacao { get { return ConfigurationManager.AppSettings["Headers.Publicacao"]; } }
        }
    }
}
