﻿// Decompiled with JetBrains decompiler
// Type: IGesDocProj.Extensions
// Assembly: IGesDocProj, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: F3FB5D40-68C2-4B8F-AEB1-03EA37FD7521
// Assembly location: \\192.168.111.69\c$\inetpub\wwwroot\IGesDoc\bin\IGesDocProj.dll

using Microsoft.SharePoint.Client;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace IArqDE
{
    public static class Extensions
    {
        public static string NoFiletypeExtension(this string s)
        {
            try
            {
                return s.Substring(0, s.LastIndexOf('.'));
            }
            catch
            {
                return s;
            }
        }

        public static SecureString ToSecureString(this string s)
        {
            SecureString secureString = new SecureString();
            foreach (char c in s)
            secureString.AppendChar(c);
            return secureString;
        }

        public static byte[] ToByteArray(this Stream s)
        {
            try
            {
                var pos = s.Position;

                byte[] buffer = new byte[16 * 1024];
                using (MemoryStream ms = new MemoryStream())
                {
                    int read;
                    while ((read = s.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, read);
                    }

                    if (s.CanSeek) s.Position = pos;

                    return ms.ToArray();
                }
            }
            catch
            {
                return null;
            }
        }

        public static byte[] ToByteArray(this string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }

        public static Stream ToStream(this byte[] b)
        {
            return (Stream) new MemoryStream(b);
        }

        public static string CalculateMD5Hash(this string input)
        {
            byte[] hash = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder stringBuilder = new StringBuilder();
            for (int index = 0; index < hash.Length; ++index)
            stringBuilder.Append(hash[index].ToString("X2"));
            return stringBuilder.ToString().ToLower();
        }
        public static ContentType GetByName(this ContentTypeCollection cts, string name)
        {
            var ctx = cts.Context;
            ctx.Load(cts, types => types.Include
           (type => type.Id, type => type.Name,
             type => type.Parent));

            ctx.ExecuteQuery();

            var res = ctx.LoadQuery(cts.Where(c => c.Name == name));
            ctx.ExecuteQuery();

            return res.FirstOrDefault();
        }
    }
}
