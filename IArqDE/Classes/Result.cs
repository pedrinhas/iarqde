﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IArqDE
{
    [DataContract]
    public class Result
    {
        [DataMember]
        public bool response { get; set; }
        [DataMember]
        public string description { get; set; }
    }
}