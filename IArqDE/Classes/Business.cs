﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;

namespace IArqDE
{
    public class Business
    {
        #region SELECT
        public ParamEntidade GetParamEntidadeBll(string sigla)
        {
            try
            {
                Model m = new Model();

                return m.GetParamEntidadeMdl(sigla);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetParamProcessoBll(string processo)
        {
            try
            {
                Model m = new Model();

                return m.GetParamProcessoMdl(processo);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<ProcessosArqdeItem> GetProcessosArqdeBll(string app)
        {
            try
            {
                Model m = new Model();

                return m.GetProcessosArqdeMdl(app);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //GetAlunoFromSIGACADMdl
        public List<AlunoItem> GetAlunoFromSIGACADBll(string numero)
        {
            try
            {
                Model m = new Model();

                return m.GetAlunoFromSIGACADMdl(numero);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public List<OutrosCursos> GetOutrosCursosBll()
        {
            try
            {
                Model m = new Model();

                return m.GetOutrosCursosMdl();
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        #endregion

        #region INSERTS
        public bool InsertLogsBll(Logs logs)
        {
            try
            {
                Model m = new Model();

                return m.InsertLogsMdl(logs);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public string InsertProcessoBll(ProcessoItem item)
        {
            try
            {
                Model m = new Model();

                return m.InsertProcessoMdl(item);    
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
                   

        }

        public string InsertProcessoAnexoBll(string numero, IEnumerable<ProcessoAnexo> value, string login, string confidencial = "0", int assinado = 0)      
        {
            try
            {
                DataTable dt = ToDataTable(value.ToList());

                Model m = new Model();

                return m.InsertProcessoAnexoMdl(numero, dt, login, confidencial, assinado);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            

        }

        #endregion

        #region utils
        public static DataTable ToDataTable<LoginCC>(IList<LoginCC> data)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(LoginCC));
            DataTable table = new DataTable();

            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }

            object[] values = new object[props.Count];

            foreach (LoginCC item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }

            return table;
        }
        #endregion

    }
}