﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IArqDE
{
    public class ParamEntidade
    {
        public string id { get; set; }
        public string login { get; set; }
        public string nome { get; set; }
        public string numMec { get; set; }
        public string categoria { get; set; }
        public string cargo { get; set; }
        public string assinatura { get; set; }

    }
}