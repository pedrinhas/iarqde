﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace IArqDE
{
    [DataContract]
    public class ProcessoItem
    {       
        [DataMember]
        public int idEntidade  {get;set;}
        [DataMember]
        public string idParamProcessos { get; set; }
        [DataMember]
        public string processo { get; set; }
        [DataMember]
        public string assunto { get; set; }
        [DataMember]
        public string texto { get; set; }
        [DataMember]
        public string loginProponente { get; set; }
        [DataMember]
        public string numMec { get; set; }
        [DataMember]
        public string nomeProponente { get; set; }
        [DataMember]
        public string categoriaProponente { get; set; }
        [DataMember]
        public string criadoPor { get; set; }
        [DataMember]
        public string nomeCriadoPor { get; set; }
        [DataMember]
        public string assinatura { get; set; }
        [DataMember]
        public string cargo { get; set; }
        [DataMember]
        public string notas { get; set; }
        [DataMember]
        public string SIGLA { get; set; }
        [DataMember]
        public string informacaoPessoal { get; set; }
        [DataMember]
        public string participante { get; set; }
        [DataMember]
        public string destinatario { get; set; }
        [DataMember]
        public string pediuValidacao { get; set; }

    }
}