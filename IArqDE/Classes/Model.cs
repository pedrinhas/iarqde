﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IArqDE
{
    public class Model
    {
        #region SELECTS
        public ParamEntidade GetParamEntidadeMdl(string sigla)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["arqde"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "stp_paramEntidade_S";
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@sigla", System.Data.SqlDbType.VarChar).Value = sigla;

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        ParamEntidade entidade = new ParamEntidade();

                        while (reader.Read())
                        {
                            entidade.id = Convert.ToString(reader["id"]);
                            entidade.login = Convert.ToString(reader["loginManager"]).Trim();
                            entidade.nome = Convert.ToString(reader["nomeManger"]).Trim();
                            entidade.cargo = Convert.ToString(reader["Cargo"]).Trim();
                            entidade.numMec = Convert.ToString(reader["numMec"]).Trim();
                            entidade.categoria = Convert.ToString(reader["categoria"]).Trim();
                            entidade.assinatura = Convert.ToString(reader["assinatura"]).Trim();
                        }


                        return entidade;
                    }
                }

            }
            catch (Exception ex)
            {

                throw new Exception("GetParamEntidadeMdl(string sigla)" + ex.ToString());
            }

        }

        public string GetParamProcessoMdl(string processo)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["arqde"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "stp_paramProcesso_S";
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@processo", System.Data.SqlDbType.VarChar).Value = processo;

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            return Convert.ToString(reader["id"]);
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                throw new Exception("GetParamProcessoMdl(string processo)" + ex.ToString());
            }

            return null;

        }

        public List<ProcessosArqdeItem> GetProcessosArqdeMdl(string app)
        {

            try
            {
                List<ProcessosArqdeItem> listData = new List<ProcessosArqdeItem>();

                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["arqde"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "stp_tipoProcessosSincronizacao_LS";
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@app", System.Data.SqlDbType.VarChar).Value = app;

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            ProcessosArqdeItem item = new ProcessosArqdeItem();
                            item.id = Convert.ToInt32(reader["id"]);
                            item.processo = Convert.ToString(reader["processo"]).Trim();
                            item.designacao = Convert.ToString(reader["designacao"]).Trim();
                            item.pagina = Convert.ToString(reader["pagina"]).Trim();
                            item.referencia = Convert.ToString(reader["referencia"]).Trim();
                            item.ativo = Convert.ToString(reader["ativo"]).Trim();
                            item.versao = Convert.ToString(reader["versao"]).Trim();
                            item.modeloPDF = Convert.ToString(reader["modeloPDF"]).Trim();
                            item.descricao = Convert.ToString(reader["descricao"]).Trim();
                            item.codigoSubSerie = Convert.ToString(reader["codigoSubSerie"]).Trim();
                            item.idSubSerie = Convert.ToString(reader["idSubSerie"]).Trim();
                            item.idParamFluxosRaiz = Convert.ToInt32(reader["idParamFluxosRaiz"]);
                            item.corpoInformacao = Convert.ToString(reader["corpoInformacao"]);
                            item.descricaoRaiz = Convert.ToString(reader["descricaoRaiz"]).Trim();

                            listData.Add(item);
                        }
                    }
                }

                return listData;
            }
            catch (Exception ex)
            {

                throw new Exception("GetProcessosArqdeMdl(string app)" + ex.ToString());
            }

            return null;

            }

        public List<AlunoItem> GetAlunoFromSIGACADMdl(string numero)
        {

            try
            {
                List<AlunoItem> listData = new List<AlunoItem>();

                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["arqde"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "getAlunoFromSIGACAD";
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@numero", System.Data.SqlDbType.VarChar).Value = numero;

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            AlunoItem item = new AlunoItem();

                            item.numMec = Convert.ToString(reader["numero"]).Trim();
                            item.nome = Convert.ToString(reader["nome"]).Trim();
                            item.login = Convert.ToString(reader["login"]).Trim();
                            item.nif = Convert.ToString(reader["nif"]).Trim();
                            item.bi = Convert.ToString(reader["bi"]).Trim();
                            item.dataNascimento = Convert.ToString(reader["datanasc"]).Trim();
                            item.codCurso = Convert.ToString(reader["codCurso"]).Trim();
                            item.curso = Convert.ToString(reader["curso"]).Trim();
                            item.dataMatricula = Convert.ToString(reader["dataMatricula"]).Trim();
                            
                            listData.Add(item);
                        }
                    }
                }

                return listData;
            }
            catch (Exception ex)
            {

                throw new Exception("GetAlunoFromSIGACADMdl(string numero)" + ex.ToString());
            }
           
        }

        public List<OutrosCursos> GetOutrosCursosMdl()
        {

            try
            {
                List<OutrosCursos> listData = new List<OutrosCursos>();

                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["gesDoc"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "stp_OutrosCurso_LS";
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            OutrosCursos item = new OutrosCursos();

                            item.id = Convert.ToInt16(reader["id"]);
                            item.curso = Convert.ToString(reader["curso"]).Trim();

                            listData.Add(item);

                        }
                    }
                }

                return listData;
            }
            catch (Exception ex)
            {

                throw new Exception("GetOutrosCursosMdl()" + ex.ToString());
            }

            
        }
        #endregion

        #region INSERTS
        public bool InsertLogsMdl(Logs logs)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["arqde"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "stp_Logs_I";
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;


                        cmd.Parameters.Add("@idProcesso", System.Data.SqlDbType.VarChar).Value = logs.idProcesso;
                        cmd.Parameters.Add("@login", System.Data.SqlDbType.VarChar).Value = logs.login;
                        cmd.Parameters.Add("@msg", System.Data.SqlDbType.VarChar).Value = logs.msg;
                        cmd.Parameters.Add("@acao", System.Data.SqlDbType.VarChar).Value = logs.acao;
                        cmd.Parameters.Add("@link", System.Data.SqlDbType.VarChar).Value = logs.link;
                        cmd.Parameters.Add("@stackTrace", System.Data.SqlDbType.VarChar).Value = logs.stackTrace;


                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("InsertLogs()" + ex.ToString());
            }

            return true;
        }

        public string InsertProcessoMdl(ProcessoItem processo)
        {
            List<ProcessoItem> listData = new List<ProcessoItem>();
            
            try
            {
                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["arqde"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "stp_Processos_I";
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@idParamProcessos", System.Data.SqlDbType.VarChar).Value = processo.idParamProcessos;
                        cmd.Parameters.Add("@processo", System.Data.SqlDbType.VarChar).Value = processo.processo;
                        cmd.Parameters.Add("@assunto", System.Data.SqlDbType.VarChar).Value = processo.assunto;
                        cmd.Parameters.Add("@texto", System.Data.SqlDbType.VarChar).Value = processo.texto;
                        cmd.Parameters.Add("@loginProponente", System.Data.SqlDbType.VarChar).Value = !String.IsNullOrWhiteSpace(processo.loginProponente) ? processo.loginProponente : "" ;
                        cmd.Parameters.Add("@numMec", System.Data.SqlDbType.VarChar).Value = processo.numMec;
                        cmd.Parameters.Add("@nomeProponente", System.Data.SqlDbType.VarChar).Value = processo.nomeProponente;
                        cmd.Parameters.Add("@categoriaProponente", System.Data.SqlDbType.VarChar).Value = !String.IsNullOrWhiteSpace(processo.categoriaProponente) ? processo.categoriaProponente : "";
                        cmd.Parameters.Add("@criadoPor", System.Data.SqlDbType.VarChar).Value = processo.criadoPor;
                        cmd.Parameters.Add("@nomeCriadoPor", System.Data.SqlDbType.VarChar).Value = processo.nomeCriadoPor;
                        cmd.Parameters.Add("@participante", System.Data.SqlDbType.VarChar).Value = !String.IsNullOrWhiteSpace(processo.participante) ? processo.participante : "";
                        cmd.Parameters.Add("@idEntidade", System.Data.SqlDbType.Int).Value = processo.idEntidade;
                        cmd.Parameters.Add("@SIGLA", System.Data.SqlDbType.VarChar).Value = processo.SIGLA;
                        cmd.Parameters.Add("@assinatura", System.Data.SqlDbType.VarChar).Value = !String.IsNullOrWhiteSpace(processo.assinatura) ? processo.assinatura : "0";
                        cmd.Parameters.Add("@cargo", System.Data.SqlDbType.VarChar).Value = !String.IsNullOrWhiteSpace(processo.cargo) ?  processo.cargo : "";
                        cmd.Parameters.Add("@destinatario", System.Data.SqlDbType.VarChar).Value = !String.IsNullOrWhiteSpace(processo.destinatario) ? processo.destinatario : "";
                        cmd.Parameters.Add("@notas", System.Data.SqlDbType.VarChar).Value = !String.IsNullOrWhiteSpace(processo.notas) ? processo.notas : "";
                        cmd.Parameters.Add("@pediuValidacao", System.Data.SqlDbType.VarChar).Value = !String.IsNullOrWhiteSpace(processo.pediuValidacao) ? processo.pediuValidacao : "0";
                        cmd.Parameters.Add("@informacaoPessoal", System.Data.SqlDbType.VarChar).Value = !String.IsNullOrWhiteSpace(processo.informacaoPessoal) ? processo.informacaoPessoal : "0";                    

                        con.Open();
                       
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            ProcessoItem items = new ProcessoItem();

                            if (!String.IsNullOrWhiteSpace(reader["numero"].ToString()))
                            {
                                return reader["numero"].ToString();
                            }
                            else
                            {
                                throw new Exception("InsertProcessoMdl(), não retornou nenhum numero de informação");
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("InsertProcessoMdl()" + ex.ToString());
            }

            return null;
        }

        public string InsertProcessoAnexoMdl(string numero, DataTable dt, string login, string confidencial, int assinado)
        {

            try
            {

                using (SqlConnection con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["arqde"].ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandText = "stp_ProcessosAnexos_I";
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        cmd.Parameters.Add("@numero", System.Data.SqlDbType.VarChar).Value = numero;

                        cmd.Parameters.Add("@ProcessosAnexos", System.Data.SqlDbType.Structured).Value = dt;

                        cmd.Parameters.Add("@login", System.Data.SqlDbType.VarChar).Value = login;

                        cmd.Parameters.Add("@confidencial", System.Data.SqlDbType.VarChar).Value = confidencial;

                        cmd.Parameters.Add("@assinado", System.Data.SqlDbType.Int).Value = assinado;

                        con.Open();

                        SqlDataReader reader = cmd.ExecuteReader();

                        while (reader.Read())
                        {
                            return reader["id"].ToString();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception("InsertProcessoAnexoMdl()" + ex.ToString());
            }

            return null;
        }
        #endregion

    }
}