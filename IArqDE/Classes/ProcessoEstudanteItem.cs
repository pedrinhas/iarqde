﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IArqDE
{
    public class ProcessoEstudanteItem
    {
        public string curso { get; set; }
        public string assunto { get; set; }
        public string texto { get; set; }
        public string numMec { get; set; }
        public string nome { get; set; }
        public string loginCriadoPor { get; set; }
        public string nomeCriadoPor { get; set; }
        public string idParamProcessos { get; set; }
    }
}