﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IArqDE
{
    public class Logs
    {
        public string idProcesso { get; set; }
        public string login { get; set; }
        public string msg { get; set; }
        public string acao { get; set; }
        public string link { get; set; }
        public string stackTrace { get; set; }
    }
}