﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IArqDE
{
    public class AlunoItem
    {
        public string numMec { get; set; }
        public string nome { get; set; }
        public string login { get; set; }
        public string nif { get; set; }
        public string bi { get; set; }
        public string dataNascimento { get; set; }
        public string codCurso { get; set; }
        public string curso { get; set; }
        public string dataMatricula { get; set; }
        public string dataFimCurso { get; set; }
        public string nota { get; set; }
        public string numero { get; set; }
    }
}