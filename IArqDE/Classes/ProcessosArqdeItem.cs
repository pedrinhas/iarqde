﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IArqDE
{
    public class ProcessosArqdeItem
    {
        public int id { get; set; }
        public string processo { get; set; }
        public string designacao { get; set; }
        public string pagina { get; set; }
        public string referencia { get; set; }
        public string ativo { get; set; }
        public string versao { get; set; }
        public string modeloPDF { get; set; }
        public string descricao { get; set; }
        public string codigoSubSerie { get; set; }
        public string idSubSerie { get; set; }
        public int idParamFluxosRaiz { get; set; }
        public string corpoInformacao { get; set; }
        public string descricaoRaiz { get; set; }
    }
}