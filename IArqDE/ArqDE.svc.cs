﻿using SharePointTools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace IArqDE
{    
    public class ArqDE : IArqDE
    {

        public string JSONData(string id)
        {
            return "Como é? O teu pedido foi: " + id;
        }

        #region GET
        
        #region REQUERIMENTOS
        public Stream GetFileVersion(string numero, string filename, string version)
        {

            try
            {
                var library = AppConfig.ListNameProcessos;

                string url = AppConfig.SharepointApplicationsUrl;
                ICredentials sharepointCredentials = SharepointTools.GetSharepointCredentials();
                byte[] bytes = SharepointTools.DownloadFileVersion(url, sharepointCredentials, library, numero, filename, version);

                if (bytes == null) return null;

                WebOperationContext.Current.OutgoingResponse.ContentLength = (long)bytes.Length;

                WebOperationContext.Current.OutgoingResponse.ContentType = GetMimeType(filename);

                return SharepointTools.ByteArrayToStream(bytes);

            }
            catch (Exception ex)
            {

                Logs logs = new Logs();
                Business b = new Business();

                logs.idProcesso = "";
                logs.login = "IArqDE";
                logs.msg = ex.ToString();
                logs.acao = "GetFileVersion(string numero, string filename, string version)";
                logs.link = "";
                logs.stackTrace = "";

                b.InsertLogsBll(logs);

                return (Stream)null;
            }
        }

        public Stream GetFile(string numero, string filename)
        {

            try
            {
                var library = AppConfig.ListNameProcessos;

                string url = AppConfig.SharepointApplicationsUrl;
                ICredentials sharepointCredentials = SharepointTools.GetSharepointCredentials();
                byte[] bytes = SharepointTools.DownloadFile(url, sharepointCredentials, library, numero, filename);

                if (bytes == null) return null;

                WebOperationContext.Current.OutgoingResponse.ContentLength = (long)bytes.Length;

                WebOperationContext.Current.OutgoingResponse.ContentType = GetMimeType(filename);

                return SharepointTools.ByteArrayToStream(bytes);

            }
            catch (Exception ex)
            {

                Logs logs = new Logs();
                Business b = new Business();

                logs.idProcesso = "";
                logs.login = "IArqDE";
                logs.msg = ex.ToString();
                logs.acao = "GetFile(string numero, string filename)";
                logs.link = "";
                logs.stackTrace = "";

                b.InsertLogsBll(logs);

                return (Stream)null;
            }
        }

        public List<FileVersionsItem> GetFileListVersion(string numero)
        {

            try
            {

                List<FileVersionsItem> lst = new List<FileVersionsItem>();

                var library = AppConfig.ListNameProcessos;

                string url = AppConfig.SharepointApplicationsUrl;
                ICredentials sharepointCredentials = SharepointTools.GetSharepointCredentials();
                var fileList = SharepointTools.GetFileListVersion(url, sharepointCredentials, library, numero);


                foreach (var fileItem in fileList)
                {

                    try
                    {
                        foreach (var versionItem in fileItem.Versions)
                        {
                            lst.Add(new FileVersionsItem { name = fileItem.Name, version = versionItem.VersionLabel.ToString() });
                        }

                        lst.Add(new FileVersionsItem { name = fileItem.Name, version = String.Format("{0}.{1}", fileItem.MajorVersion.ToString(), fileItem.MinorVersion.ToString()) });
                    }
                    catch (Exception)
                    {
                        lst.Add(new FileVersionsItem { name = fileItem.Name, version = String.Format("{0}.{1}", fileItem.MajorVersion.ToString(), fileItem.MinorVersion.ToString()) });
                    }

                }

                return lst;
            }
            catch (Exception ex)
            {

                Logs logs = new Logs();
                Business b = new Business();

                logs.idProcesso = "";
                logs.login = "IArqDE";
                logs.msg = ex.ToString();
                logs.acao = "GetFileListVersion(string numero)";
                logs.link = "";
                logs.stackTrace = "";

                b.InsertLogsBll(logs);

                return null;
            }
        }

        public List<ProcessoAnexo> GetFileList(string numero)
        {

            try
            {

                List<ProcessoAnexo> lst = new List<ProcessoAnexo>();

                var library = AppConfig.ListNameProcessos;

                string url = AppConfig.SharepointApplicationsUrl;
                ICredentials sharepointCredentials = SharepointTools.GetSharepointCredentials();
                var fileList = SharepointTools.GetFileList(url, sharepointCredentials, library, numero);


                foreach (var fileItem in fileList)
                {
                   
                    lst.Add(new ProcessoAnexo { anexo = fileItem });

                }

                return lst;
            }
            catch (Exception ex)
            {

                Logs logs = new Logs();
                Business b = new Business();

                logs.idProcesso = "";
                logs.login = "IArqDE";
                logs.msg = ex.ToString();
                logs.acao = "GetFileList(string numero)";
                logs.link = "";
                logs.stackTrace = "";

                b.InsertLogsBll(logs);

                return null;
            }
        }

        #endregion

        #region Dados Alunos
        
        public List<AlunoItem> GetAlunoFromSIGACAD(string numero)
        {
            try
            {
                Business b = new Business();

                return b.GetAlunoFromSIGACADBll(numero);

            }
            catch (Exception ex)
            {

                Logs logs = new Logs();
                Business b = new Business();

                logs.idProcesso = "";
                logs.login = "IArqDE";
                logs.msg = ex.ToString();
                logs.acao = "GetAlunoFromSIGACAD(string numero)";
                logs.link = "";
                logs.stackTrace = "";

                b.InsertLogsBll(logs);

                return null;
            }
        }

        public List<OutrosCursos> GetOutrosCursos()
        {
            try
            {
                Business b = new Business();

                return b.GetOutrosCursosBll();
            }
            catch (Exception ex)
            {

                Logs logs = new Logs();
                Business b = new Business();

                logs.idProcesso = "";
                logs.login = "IArqDE";
                logs.msg = ex.ToString();
                logs.acao = "GetOutrosCursos()";
                logs.link = "";
                logs.stackTrace = "";

                b.InsertLogsBll(logs);

                return null;
            }
        }


        #endregion


        #region GENERICO

        public List<ProcessosArqdeItem> GetTiposProcessos(string app)
        {
            try
            {
                Business b = new Business();
                return b.GetProcessosArqdeBll(app);

            }
            catch (Exception ex)
            {

                Logs logs = new Logs();
                Business b = new Business();

                logs.idProcesso = "";
                logs.login = "IArqDE";
                logs.msg = ex.ToString();
                logs.acao = "GetTiposProcessos(string app)";
                logs.link = "";
                logs.stackTrace = "";

                b.InsertLogsBll(logs);

                return null;
            }

        }

        #endregion

        #endregion

        #region POST


        #region GENERICO

        public Result InsertProcessoEstudante(Stream request)
        {

            Logs logs = new Logs();
            Result result = new Result();
            string numInf = "";

            try
            {

                var reader = new StreamReader(request);
                var reqText = reader.ReadToEnd();

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                ProcessoEstudanteItem processoEstudante = serializer.Deserialize<ProcessoEstudanteItem>(reqText);

                Business b = new Business();
                ProcessoItem processo = new ProcessoItem();

                string currentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;

                var sigla = "PDE";
                var entidade = b.GetParamEntidadeBll(sigla);

                processo.idParamProcessos = processoEstudante.idParamProcessos;

                processo.SIGLA = sigla;
                processo.idEntidade = Convert.ToInt32(entidade.id);

                processo.processo = processoEstudante.curso;
                processo.assunto = processoEstudante.assunto;
                processo.texto = processoEstudante.texto;
                processo.numMec = processoEstudante.numMec;
                processo.nomeProponente = processoEstudante.nome;
                processo.criadoPor = processoEstudante.loginCriadoPor;
                processo.nomeCriadoPor = processoEstudante.nomeCriadoPor;
                numInf = b.InsertProcessoBll(processo);

                if (!String.IsNullOrWhiteSpace(numInf))
                {
                    result.response = true;
                    result.description = numInf;

                    return result;
                }
                else
                {
                    result.response = false;
                    return result;
                }

            }
            catch (Exception ex)
            {

                logs.idProcesso = numInf ?? "";
                logs.login = "IArqDE";
                logs.msg = "InsertProcessoEstudante";
                logs.acao = "inserir nova processo estudante";
                logs.link = "";
                logs.stackTrace = ex.ToString();

                result.response = false;

                return result;
            }

        }

        public Result InsertAnexoProcessoEstudante(Stream anexo, string numInf, string login, string nomeAnexo)
        {
            Logs logs = new Logs();
            Result result = new Result();
            try
            {
                Business b = new Business();
                List<ProcessoAnexo> list = new List<ProcessoAnexo>();
                ProcessoItem processo = new ProcessoItem();

                string fileName = (HttpContext.Current.Request.Headers["Filename"] ?? "").ToString();
                var streamContent = anexo.ToByteArray();


                if (streamContent.Count() == 0)
                {
                    logs.idProcesso = numInf;
                    logs.login = "IArqDE";
                    logs.msg = "InsertAnexoProcessoEstudante";
                    logs.acao = "inserir anexo";
                    logs.link = "";
                    logs.stackTrace = "";

                    result.response = false;
                    result.description = "stream content = 0";
                    return result;
                }
                else if (!String.IsNullOrWhiteSpace(SharepointTools.UploadFile(AppConfig.SharepointApplicationsUrl, SharepointTools.GetSharepointCredentials(), AppConfig.ListNameProcessos, numInf, streamContent, nomeAnexo, SharepointTools.ItemExistsStrategy.Overwrite)))
                {
                    list.Add(new ProcessoAnexo { anexo = nomeAnexo });

                    if (!String.IsNullOrWhiteSpace(b.InsertProcessoAnexoBll(numInf, list, login)))
                    {

                        result.response = true;

                        return result;
                    }
                    else
                    {
                        logs.idProcesso = numInf;
                        logs.login = "IArqDE";
                        logs.msg = "InsertAnexoProcessoEstudante";
                        logs.acao = "inserir anexo";
                        logs.link = "";
                        logs.stackTrace = "";

                        result.response = false;

                        return result;
                    }
                }
                else
                {
                    logs.idProcesso = numInf;
                    logs.login = "IArqDE";
                    logs.msg = "InsertAnexoProcessoEstudante -> SharepointTools.UploadFile";
                    logs.acao = "inserir anexo";
                    logs.link = "";
                    logs.stackTrace = "";

                    result.response = false;

                    return result;
                }
            }
            catch (Exception ex)
            {

                logs.idProcesso = numInf;
                logs.login = "IArqDE";
                logs.msg = "InsertAnexoProcessoEstudante";
                logs.acao = "inserir anexo";
                logs.link = "";
                logs.stackTrace = ex.ToString();

                result.response = false;

                return result;
            }
        }


        #endregion


        #region REQUERIMENTOS
        //public Result InsertInformacaoRequerimento(string curso, string assunto, string texto, string numMec, string nome, string loginCriadoPor, string nomeCriadoPor)
        //{

        //    Logs logs = new Logs();
        //    Result result = new Result();
        //    string numInf = "";

        //    try
        //    {

        //        Business b = new Business();
        //        ProcessoItem processo = new ProcessoItem();

        //        string currentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;

        //        var sigla = "RO";
        //        var entidade = b.GetParamEntidadeBll(sigla);

        //        processo.idParamProcessos = GetProcesso(currentMethod);

        //        processo.SIGLA = sigla;
        //        processo.idEntidade = Convert.ToInt32(entidade.id);

        //        processo.processo = curso;
        //        processo.assunto = assunto;
        //        processo.texto = texto;              
        //        processo.numMec = numMec;
        //        processo.nomeProponente = nome;
        //        processo.criadoPor = loginCriadoPor;
        //        processo.nomeCriadoPor = nomeCriadoPor;             
        //        numInf = b.InsertProcessoBll(processo);                

        //        if (!String.IsNullOrWhiteSpace(numInf))
        //        {
        //            result.response = true;
        //            result.description = numInf;

        //            return result;
        //        }
        //        else
        //        {
        //            result.response = false;                        
        //            return result;
        //        }

        //    }
        //    catch (Exception ex)
        //    {

        //        logs.idProcesso = numInf ?? assunto;
        //        logs.login = "IArqDE";
        //        logs.msg = "InsertInformacaoRequerimento";
        //        logs.acao = "inserir nova informação";
        //        logs.link = "";
        //        logs.stackTrace = ex.ToString();

        //        result.response = false;

        //        return result;
        //    }
        //}

        public Result InsertInformacaoRequerimento(Stream request)
        {

            Logs logs = new Logs();
            Result result = new Result();
            string numInf = "";

            try
            {

                var reader = new StreamReader(request);
                var reqText = reader.ReadToEnd();

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                RequerimentoItem requerimento = serializer.Deserialize<RequerimentoItem>(reqText);                             

                Business b = new Business();
                ProcessoItem processo = new ProcessoItem();

                string currentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;

                var sigla = "RO";
                var entidade = b.GetParamEntidadeBll(sigla);

                processo.idParamProcessos = GetProcesso(currentMethod);

                processo.SIGLA = sigla;
                processo.idEntidade = Convert.ToInt32(entidade.id);

                processo.processo = requerimento.curso;
                processo.assunto = requerimento.assunto;
                processo.texto = requerimento.texto;
                processo.numMec = requerimento.numMec;
                processo.nomeProponente = requerimento.nome;
                processo.criadoPor = requerimento.loginCriadoPor;
                processo.nomeCriadoPor = requerimento.nomeCriadoPor;
                numInf = b.InsertProcessoBll(processo);

                if (!String.IsNullOrWhiteSpace(numInf))
                {
                    result.response = true;
                    result.description = numInf;

                    return result;
                }
                else
                {
                    result.response = false;
                    return result;
                }

            }
            catch (Exception ex)
            {

                logs.idProcesso = numInf ?? "";
                logs.login = "IArqDE";
                logs.msg = "InsertInformacaoRequerimento";
                logs.acao = "inserir nova informação";
                logs.link = "";
                logs.stackTrace = ex.ToString();

                result.response = false;

                return result;
            }
         
        }

        public Result InsertAnexoRequerimento(Stream anexo, string numInf, string login, string nomeAnexo)
        {
            Logs logs = new Logs();
            Result result = new Result();
            try
            {
                Business b = new Business();
                List<ProcessoAnexo> list = new List<ProcessoAnexo>();
                ProcessoItem processo = new ProcessoItem(); 

                string fileName = (HttpContext.Current.Request.Headers["Filename"] ?? "").ToString();
                var streamContent = anexo.ToByteArray();


                if (streamContent.Count() == 0)
                {
                    logs.idProcesso = numInf;
                    logs.login = "IArqDE";
                    logs.msg = "InsertAnexoRequerimento";
                    logs.acao = "inserir anexo";
                    logs.link = "";
                    logs.stackTrace = "";

                    result.response = false;
                    result.description = "stream content = 0";
                    return result;   
                }
                else if (!String.IsNullOrWhiteSpace(SharepointTools.UploadFile(AppConfig.SharepointApplicationsUrl, SharepointTools.GetSharepointCredentials(), AppConfig.ListNameProcessos, numInf, streamContent, nomeAnexo, SharepointTools.ItemExistsStrategy.Overwrite)))
                {
                    list.Add(new ProcessoAnexo { anexo = nomeAnexo });

                    if (!String.IsNullOrWhiteSpace(b.InsertProcessoAnexoBll(numInf, list, login)))
                    {

                        result.response = true;

                        return result;               
                    }
                    else
                    {
                        logs.idProcesso = numInf;
                        logs.login = "IArqDE";
                        logs.msg = "InsertAnexoRequerimento";
                        logs.acao = "inserir anexo";
                        logs.link = "";
                        logs.stackTrace = "";

                        result.response = false;

                        return result;                         
                    }
                }
                else
                {
                    logs.idProcesso = numInf;
                    logs.login = "IArqDE";
                    logs.msg = "InsertAnexoRequerimento -> SharepointTools.UploadFile";
                    logs.acao = "inserir anexo";
                    logs.link = "";
                    logs.stackTrace = "";

                    result.response = false;

                    return result;       
                }
            }
            catch (Exception ex)
            {

                logs.idProcesso = numInf;
                logs.login = "IArqDE";
                logs.msg = "InsertAnexoRequerimento";
                logs.acao = "inserir anexo";
                logs.link = "";
                logs.stackTrace = ex.ToString();

                result.response = false;

                return result;       
            }
        }

        public Result UpdateAnexoRequerimento(Stream anexo, string numInf, string login, string nomeAnexo)
        {
            Logs logs = new Logs();
            Result result = new Result();
            try
            {
                Business b = new Business();
                List<ProcessoAnexo> list = new List<ProcessoAnexo>();
                ProcessoItem processo = new ProcessoItem();

                string fileName = (HttpContext.Current.Request.Headers["Filename"] ?? "").ToString();
                var streamContent = anexo.ToByteArray();
               
                var file = streamContent.Count() != 0 ? SharepointTools.UploadFileVersion(AppConfig.SharepointApplicationsUrl, SharepointTools.GetSharepointCredentials(), AppConfig.ListNameProcessos, numInf, streamContent, nomeAnexo) : null;
              
                if (streamContent.Count() == 0)
                {
                    logs.idProcesso = numInf;
                    logs.login = "IArqDE";
                    logs.msg = "InsertAnexoRequerimento";
                    logs.acao = "inserir anexo";
                    logs.link = "";
                    logs.stackTrace = "";

                    result.response = false;
                    result.description = "stream content = 0";
                    return result;
                }
                else if (file != null)
                {
                    var version = file.MajorVersion;
                    list.Add(new ProcessoAnexo { anexo = nomeAnexo });

                    if (!String.IsNullOrWhiteSpace(b.InsertProcessoAnexoBll(numInf, list, login)))
                    {

                        result.response = true;
                        result.description = version.ToString();
                        return result;
                    }
                    else
                    {
                        logs.idProcesso = numInf;
                        logs.login = "IArqDE";
                        logs.msg = "InsertAnexoRequerimento";
                        logs.acao = "inserir anexo";
                        logs.link = "";
                        logs.stackTrace = "";

                        result.response = false;

                        return result;
                    }
                }
                else
                {
                    logs.idProcesso = numInf;
                    logs.login = "IArqDE";
                    logs.msg = "InsertAnexoRequerimento -> SharepointTools.UploadFile";
                    logs.acao = "inserir anexo";
                    logs.link = "";
                    logs.stackTrace = "";

                    result.response = false;

                    return result;
                }
            }
            catch (Exception ex)
            {

                logs.idProcesso = numInf;
                logs.login = "IArqDE";
                logs.msg = "InsertAnexoRequerimento";
                logs.acao = "inserir anexo";
                logs.link = "";
                logs.stackTrace = ex.ToString();

                result.response = false;

                return result;
            }
        }
        #endregion

        #endregion

        #region utility
        private string GetProcesso(string metodo)
        {
            try
            {
                Business b = new Business();

                switch (metodo)
                {
                    case "InsertInformacaoRequerimento":
                        return b.GetParamProcessoBll("requerimentoOnline");
                    break;

                    default:
                        throw new Exception("GetProcesso() - processo não existe");
                    break;
                }
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }

        public string GetMimeType(string filename)
        {
            try
            {
                string mime = "";

                string extension = filename.Substring(filename.LastIndexOf("."), filename.Length - filename.LastIndexOf("."));

                Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(extension);
                mime = key.GetValue("Content Type").ToString();

                return mime;
            }
            catch (Exception)
            {
                if (filename.EndsWith("pdf"))
                {
                    return System.Net.Mime.MediaTypeNames.Application.Pdf;
                }
                return "application/octet-stream";
            }
        }
        #endregion


        
    }
}
